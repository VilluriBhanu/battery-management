/*
 * main.c
 *
 *  Created on: Jan 23, 2022
 *      Author: BXV0523
 *
 *  Dedicated 60V Pack SW source code
 *
 */


#include "Include/global.h"
#include "Include/HAL_msp430_fr_2433.h"
#include "Include/uart.h"

#include <stdbool.h>
#include <stddef.h>
bool g_wake = true;

int main(void)
{

    HAL_gpio_init();
    HAL_clk_init();
    HAL_uart_init_half();

    while(g_wake);
    {
        Hal_set_dsg_fet_en();
    }


}
