#ifndef UART_h_
#define UART_h_

#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "HAL_msp430_fr_2433.h"

#define CIRC_BBUF_DEF(x,y)                \
    uint8_t x##_data_space[y];            \
    circ_buff x = {                     \
        .buffer = x##_data_space,         \
        .head = 0,                        \
        .tail = 0,                        \
        .count= 0,                         \
        .length = y                       \
    }



typedef struct {
    uint8_t * buffer;
    uint8_t head;
    uint8_t tail;
    uint8_t count;
    const uint8_t length;
}circ_buff;

CIRC_BBUF_DEF(mycrc,15);
void circ_buff_load(circ_buff *c, uint8_t data)
{
    uint8_t end = c->tail;
    if(c->count && (end % c->length) == c->head)
    {
        c->head = (c->head + 1 ) %c->length;
        c->count --;
      }

      c->buffer[c->tail] = data ;
      c->tail = (c->tail+1) % c->length;
      c->count ++;
      }

void transmit_circ_buff(circ_buff *c)
{

     uint8_t start = c->head ;
      uint8_t end = c->tail ;
       int i, count = 0;
       for(i = start; count < c->length; i = (i + 1)%c->length)
       {
           while(!(UCA1IFG & UCTXIFG));
              {
                  UCA1TXBUF = c->buffer[i];
                      count++;
              }
         if(i == (end - 1))
         {
           break;
         }
       }

    }


#endif
