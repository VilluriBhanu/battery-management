#ifndef MSP430_FR_2433_h_
#define MSP430_FR_2433_h_

/*
     * Rev A.0 Hardware configurations
     * Pin 1.2 ---> SDA_MCU -> Special Function
     * Pin 1.3 ---> SCL_MCU -> Special Function
     * Pin 1.4 ---> UART_BLE_TX ----> Special Function
     * Pin 1.5 ---> UART_BLE_RX ---- -> Special Function
     * Pin 1.6 ---> NTC_ADC_EN ----- Output
     * Pin 1.7 ---> NTC_ADC ------- -> Special Function (ADC Pin)
     *
     * Pin 2.0 ---> C1-C1_INT ---- Input
     * Pin 2.1 ---> TH_Pulse_INT ---- Input
     * Pin 2.2 ---> INT_AFE_MCU ---- Output
     * Pin 2.3 ---> PF_MCU
     * Pin 2.4 ---> ID_En ------ Output
     * Pin 2.5 ---> UART_EXT
     * Pin 2.6 ---> Unused
     * Pin 2.7 ---> DSG_FET_EN ---- Output
     *
     * Pin 3.0 ---> TH_EN ------ Output
     * Pin 3.1 ---> AFE_WAKE ----- Output
     * Pin 3.2 ---> 2ND_OVP_OUTPUT ---- Input
     */

#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

void MSP430FR2433_gpio_init()
{

    // PIN 1

    /*
     *   Set direction of IO's in Port1
     *   NTC_ADC_EN
    */
     P1DIR= 0b01010000;

     /*
         Setting BIT6 NTC_ADC_EN as Output
     */

     P1DIR |= (BIT6);

    /*
        Set direction of IO's in Port2
    */
    P2DIR = 0b11000100;

    /*
        Set direction of IO's in Port3
    */
    P3DIR = 0b011;
    // To enable pull up resistors
    P3REN = 0b100;
    // Pull up resistors for inputs
    P3OUT = 0b100;

    // Disable the GPIO power-on default high-impedance mode
     // to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}

  // Set DSG_FET_EN
 void MSP430FR2433_set_DSG_FET_EN(void)
 {
     P2OUT |= BIT7;
 }

 // Clear DSG_FET_EN
 void MSP430FR2433_clear_DSG_FET_EN(void)
  {
      P2OUT &= ~(BIT7);
  }

 // Set ID_EN
 void MSP430FR2433_set_ID_EN(void)
  {
      P2OUT |= BIT6;
  }

 // Clear ID_EN
 void MSP430FR2433_clear_ID_EN(void)
   {
       P2OUT &= ~(BIT6);
   }

 void MSP430FR2433_clk_init()
{
    // Configure one FRAM waitstate as required by the device datasheet for MCLK
        // operation beyond 8MHz _before_ configuring the clock system.
      FRCTL0 = FRCTLPW | NWAITS_1;
      // disable FLL

    __bis_SR_register(SCG0);
    // Set REFO as FLL reference source
       CSCTL3 |= SELREF__REFOCLK;
       // clear DCO and MOD registers
       CSCTL0 = 0;
       // Clear DCO frequency select bits first
       CSCTL1 &= ~(DCORSEL_7);
       // Set DCO = 16MHz
       CSCTL1 |= DCORSEL_5;
       // DCOCLKDIV = 16MHz
       CSCTL2 = FLLD_0 + 487;
       // Required to set the configurations
       __delay_cycles(3);
       // enable FLL
       __bic_SR_register(SCG0);

       // set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
        // default DCOCLKDIV as MCLK and SMCLK source
       CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;

       // Divide the SMCLK by 2,to obtain SMCLK= 8MHz
       CSCTL5 = DIVS_1;

}

// maximum 32ms
 void MSP430FR2433_time_ms(unsigned int s)
{
     // TACCR0 interrupt enabled
    TA0CCTL0 |= CCIE;
    /*
     * SMCLK is enabled - 0b
     * Divide by 8 -
     */

    TA0CCR0 = 20000*s;

    // SMCLK, Divide by 8, Up mode, Interrupt enabled

    TA0CTL |= TASSEL__SMCLK | ID_3 | MC__UP | TAIE;

}

void MSP430FR2433_i2c_init()
{

}

int MSP430FR2433_i2c_read(int address, int numofbytes)
{

    return &buffer;
}

void MSP430FR2433_i2c_write(int addrss, int numberofbytes, int *buffer)
{

}
void MSP430FR2433_uart_full_duplex_init(void)
{

}


 void MSP430FR2433_uart_half_duplex_init()
{
     // Configure UART
          UCA1CTLW0 |= UCSWRST;
           UCA1CTLW0 |= UCSSEL__SMCLK;

                         // Baud Rate calculation example
                        // 8000000/(16*9600) = 52.083
                         // Fractional portion = 0.083
                         // User's Guide Table 14-4: UCBRSx = 0x49
                        // UCBRFx = int ( (52.083-52)*16) = 1

           // Using table for 115200 baud rate
                        UCA1BR0 = 04;
                             UCA1BR1 = 0x00;
                             UCA1MCTLW |= 0x5500 | UCOS16 | UCBRF_5;



           // 16000000/1 = 16M [ 16M SMCLK with 750k baud rate ]
         /*  UCA1BR0 = 1;
               UCA1BR1 = 0;

               UCA1MCTLW = 69 << 8 | UCBRF_5 | UCOS16;   // Settings from Baudrate Calculator
               */

                             // Enable USCI_A0 TX interrupt
                             UCA1IE |= UCTXIE;
                             UCA1CTLW0 &= ~UCSWRST;

}

void MSP430FR2433_adc_init(void)
{

}

// All Function pointer need to be declared below
volatile HAL_Gpio_Init             HAL_gpio_init                      = MSP430FR2433_gpio_init ;


#endif
